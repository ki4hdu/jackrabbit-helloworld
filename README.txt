Hello

I have this setup in IntelliJ with a library that includes "jackrabbit-standalone-2.8.0" (it's not pulled in via the pom for reasons that I don't completely understand and don't want to worry with at the moment).

I have a run configuration setup with VM Options:
  -Xmx512m -Dlogback.configurationFile=file:/Users/mckeehan/src/JackRabbit-HelloWorld/target/classes/logback.xml]
this increases the max heap size and provides an override for the provided logback.xml file in jackrabbit-standalone-2.8.0 to make it log much less (and all logging goes to the console with this config).

