package wjm;

import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by mckeehan on 10/10/14.
 */
public class FindEmptySeries {
    private final File newTextFile = new File("/tmp/empty_series.txt");
    private final FileWriter fw;
    private final SimpleCredentials creds = new SimpleCredentials("161151", "Wkq2P#fu".toCharArray());
    private final Repository repository;
    private final String repoUrl = "http://author.hgtv-prod2.sni.hgtv.com/crx/server";
    private final Session session;

    public FindEmptySeries() throws IOException, RepositoryException {
        fw = new FileWriter(newTextFile);

        this.repository = JcrUtils.getRepository(repoUrl);
        session = repository.login(creds);
    }

    /**
     * The main entry point of the example application.
     *
     * @param args command line arguments (ignored)
     * @throws Exception if an error occurs
     */
    public static void main(String[] args) throws Exception {
        FindEmptySeries firstHop = new FindEmptySeries();
        firstHop.run();
        firstHop.finish();
    }

    private void finish() throws IOException {
        fw.close();
    }

    public void run() throws Exception {
        ArrayList<Runner> myRunners = new ArrayList<Runner>();
        // Obtain the query manager for the session via the workspace ...
        javax.jcr.query.QueryManager queryManager = session.getWorkspace().getQueryManager();

        // Create a query object ...
        String queryString = "select * from nt:base where jcr:path like '/content/hgtv-com/en/shows/on-tv/%' and sni:assetType = 'SERIES' ";
        Query query = queryManager.createQuery(queryString, Query.SQL);
        Runner runner = new Runner(query);
        runner.run();

        //wait or all runners to complete
        for (Runner r : myRunners) {
            r.join();
        }

        session.logout();
    }

    private class Runner extends Thread {
        private Query query;

        private Runner(Query query) {
            this.query = query;
        }

        public void run() {
            RowIterator rows;
            // Execute the query and get the results ...
            try {
                QueryResult result = this.query.execute();
                rows = result.getRows();
                Long howMany = rows.getSize();
                fw.append(howMany.toString());
                fw.append('\n');
                while (rows.hasNext()) {
                    Row row = rows.nextRow();
                    if (row != null) {
                        Node n = row.getNode();
                        if( n != null ) {
                            Node parent = n.getParent();
                            if( parent != null ) {
                                Long count = parent.getNodes().getSize();
                                if( count == 1 ) {
                                    fw.append(parent.getPath())
                                            .append('\t')
                                            .append(n.hasProperty("sni:assetLink") ? n.getProperty("sni:assetLink").getString() : "WJM")
                                            .append('\n')
                                    ;
                                }
                            }
                        }
                    }
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private String getPropertyValues(Node n, String property) throws RepositoryException, IOException {
            StringBuilder sb = new StringBuilder();
            Property p = n.getProperty(property);
            //fw.append(p.getString());

            PropertyIterator pi = n.getProperties(property);
             while (pi.hasNext()) {
                 p = pi.nextProperty();
                 sb.append(p.getString());
             }

            return sb.toString();
         }
    }
}
