package wjm;

import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by mckeehan on 10/10/14.
 */
public class GenerateVideoUrls {
    private final File newTextFile = new File("/tmp/single_video_pages.txt");
    private final FileWriter fw;
    private final SimpleCredentials creds = new SimpleCredentials("161151", "M!i6qUPA".toCharArray());
    private final Repository repository;
    private final String repoUrl = "http://author1.hgtv-prod2.sni.hgtv.com:4502/crx/server";
    private final Session session;

    public GenerateVideoUrls() throws IOException, RepositoryException {
        fw = new FileWriter(newTextFile);

        this.repository = JcrUtils.getRepository(repoUrl);
        session = repository.login(creds);
    }

    /**
     * The main entry point of the example application.
     *
     * @param args command line arguments (ignored)
     * @throws Exception if an error occurs
     */
    public static void main(String[] args) throws Exception {
        GenerateVideoUrls firstHop = new GenerateVideoUrls();
        firstHop.run();
        firstHop.finish();
    }

    private void finish() throws IOException {
        fw.close();
    }

    public void run() throws Exception {
        ArrayList<Runner> myRunners = new ArrayList<Runner>();
        // Obtain the query manager for the session via the workspace ...
        javax.jcr.query.QueryManager queryManager = session.getWorkspace().getQueryManager();

        // Create a query object ...
        String expression = "select * from nt:base where jcr:path like '/etc/sni-asset/hgtv/videos/BUCKET/61/611/6113/61134/%'  and jcr:primaryType = 'cq:PageContent'";

        String[] buckets = {"0", "6", "7", "8", "9"};
        for (String bucket : buckets) {
            String queryString = expression.replaceAll("BUCKET", bucket);

            Query query = queryManager.createQuery(queryString, Query.SQL);
            Runner runner = new Runner(query);
            runner.run();
        }

        //wait or all runners to complete
        for (Runner r : myRunners) {
            r.join();
        }

        session.logout();
    }

    private class Runner extends Thread {
        private Query query;

        private Runner(Query query) {
            this.query = query;
        }

        public void run() {
            RowIterator rows;
            // Execute the query and get the results ...
            try {
                QueryResult result = this.query.execute();
                rows = result.getRows();
                while (rows.hasNext()) {
                    Row row = rows.nextRow();
                    if (row != null) {
                        Node n = row.getNode();
                        if (n != null) {
                            synchronized (fw) {
                                fw.append(n.hasProperty("sni:psaId") ? n.getProperty("sni:psaId").getString() : "WJM")
                                        .append('\t')
                                        .append(generateUrl(n))
                                        .append('\n')
                                ;
                            }
                        }
                    }
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private String generateUrl(Node n) throws RepositoryException {
            StringBuilder sb = new StringBuilder();
            String title = "WJM";
            if (n.hasProperty("jcr:title")) {
                title = n.getProperty("jcr:title").getString();
            } else {
                if (n.hasProperty("sni:sourceTitle")) {
                    title = n.getProperty("sni:sourceTitle").getString();
                }
            }

            sb.append("http://videos.hgtv.com/video/")
                    .append(encodeTitle(title.toLowerCase()))
                    .append("-")
                    .append(n.hasProperty("sni:psaId") ? n.getProperty("sni:psaId").getString() : "WJM")
            ;

            return sb.toString();
        }

        public static final String SPACE_ENCODED = "-";
        public static final String SPACE_ACTUAL = " ";
        public static final String HYPHEN_ENCODED = "_";
        public static final String HYPHEN_ACTUAL = "-";

        public String encodeTitle(String title) {
            title = title.replaceAll(HYPHEN_ACTUAL, HYPHEN_ENCODED);
            title = title.replaceAll(SPACE_ACTUAL, SPACE_ENCODED);
            String sanitizedTitle = "";

            // RR: remove all control characters while encoding
            for (int i = 0; i < title.length(); i++) {
                if (title.charAt(i) >= 32) sanitizedTitle += title.charAt(i);
            }
            return encodeURI(sanitizedTitle);
        }

        public String encodeURI(String uri) {
            try {
                uri = URLEncoder.encode(uri, "UTF-8");
                uri = uri.replaceAll("\\+", "%20"); // return '%20' instead of '+' for white space.
            } catch (UnsupportedEncodingException e) {
                // SHOULD NEVER HAPPEN.
                //LOG.error("Error decoding '{}'", uri, e);
            }

            return uri;
        }
    }
}
