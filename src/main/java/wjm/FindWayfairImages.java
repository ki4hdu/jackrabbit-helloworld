package wjm;

import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.jcr.query.*;
import java.io.IOException;

/**
 * Created by mckeehan on 10/10/14.
 */
public class FindWayfairImages {
    private final SimpleCredentials creds = new SimpleCredentials("161151", "M!i6qUPA".toCharArray());
    private final Repository repository;
    private final String repoUrl = "http://author.hgtv-prod2.sni.hgtv.com/crx/server";
    private final Session session;

    public FindWayfairImages() throws IOException, RepositoryException {
        this.repository = JcrUtils.getRepository(repoUrl);
        session = repository.login(creds);
    }

    /**
     * The main entry point of the example application.
     *
     * @param args command line arguments (ignored)
     * @throws Exception if an error occurs
     */
    public static void main(String[] args) throws Exception {
        FindWayfairImages firstHop = new FindWayfairImages();
        firstHop.run();
    }

    public void run() throws Exception {
        // Obtain the query manager for the session via the workspace ...
        QueryManager queryManager = session.getWorkspace().getQueryManager();

        // Create a query object ...
        String queryString;
        Query query;
        // Execute the query and get the results ...
        RowIterator rows;
        QueryResult brokenPages;
        Row brokenRow;
        Node brokenContentNode;
        int count_found = 0;
        int count_fixed = 0;

        queryString = "/jcr:root/content/dam/images/hgtv/fullset hgtv-com/en/shows/on-tv//*[jcr:contains(@assetDescription, 'jcr:description')]";
        query = queryManager.createQuery(queryString, Query.XPATH);
        brokenPages = query.execute();
        rows = brokenPages.getRows();

        System.out.printf("Num rows: %d\n", rows.getSize());
        while (rows.hasNext()) {
            count_found++;
            brokenRow = rows.nextRow();
            if (brokenRow != null) {
                brokenContentNode = brokenRow.getNode();
                if (brokenContentNode != null) {
                    System.out.println(brokenContentNode.getPath());
                }
            }
        }
        System.out.printf("Found: %d\nFixed: %d", count_found, count_fixed);
        session.save();
        session.logout();
    }

}
