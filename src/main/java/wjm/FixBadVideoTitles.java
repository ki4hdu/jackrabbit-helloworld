package wjm;

import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.jcr.query.*;
import java.io.IOException;

/**
 * Created by mckeehan on 10/10/14.
 */
public class FixBadVideoTitles {
    private final SimpleCredentials creds = new SimpleCredentials("161151", "Wkq2P#fu".toCharArray());
    private final Repository repository;
    private final String repoUrl = "http://author.hgtv-prod2.sni.hgtv.com/crx/server";
    private final Session session;

    public FixBadVideoTitles() throws IOException, RepositoryException {
        this.repository = JcrUtils.getRepository(repoUrl);
        session = repository.login(creds);
    }

    /**
     * The main entry point of the example application.
     *
     * @param args command line arguments (ignored)
     * @throws Exception if an error occurs
     */
    public static void main(String[] args) throws Exception {
        FixBadVideoTitles firstHop = new FixBadVideoTitles();
        firstHop.run();
    }

    public void run() throws Exception {
        // Obtain the query manager for the session via the workspace ...
        QueryManager queryManager = session.getWorkspace().getQueryManager();

        // Create a query object ...
        String queryString;
        Query query;
        // Execute the query and get the results ...
        RowIterator rows;
        QueryResult brokenPages;
        Row brokenRow;
        Node brokenContentNode;
        Node parentContentNode;
        int count_found = 0;
        int count_fixed = 0;

        queryString = "select * from nt:base where jcr:path like '/etc/sni-asset/hgtv/videos/%' and ( jcr:title like '%\n' or jcr:title like '%\t' or jcr:title like '% ')";
        query = queryManager.createQuery(queryString, Query.SQL);
        brokenPages = query.execute();
        rows = brokenPages.getRows();

        while (rows.hasNext()) {
            count_found++;
            brokenRow = rows.nextRow();
            if (brokenRow != null) {
                brokenContentNode = brokenRow.getNode();
                if (brokenContentNode != null) {
                    if( brokenContentNode.hasProperty("jcr:title")) {

                        String title = brokenContentNode.getProperty("jcr:title").getString();
                        brokenContentNode.setProperty("jcr:title", title.trim());

                        System.out.printf("|%s| -- |%s|\n", title, title.trim());
                        count_fixed++;
                        System.out.println(brokenContentNode.getPath());
                    }
                }
            }
        }
        System.out.printf("Found: %d\nFixed: %d", count_found, count_fixed);
        session.save();
        session.logout();
    }

}
