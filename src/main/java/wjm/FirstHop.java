package wjm;

import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * First hop example. Logs in to a content repository and prints a
 * status message.
 */
public class FirstHop {
    private final File newTextFile = new File("/tmp/images.txt");
    private final FileWriter fw;
    private final SimpleCredentials creds = new SimpleCredentials("admin", "admin".toCharArray());
    private final Repository repository;
    private final String repoUrl = "http://author1.hgtv-qa2.sni.hgtv.com:4502/crx/server";
    private final Session session;

    public FirstHop() throws IOException, RepositoryException {
        fw = new FileWriter(newTextFile);

        this.repository = JcrUtils.getRepository(repoUrl);
        session = repository.login(creds);
    }

    /**
     * The main entry point of the example application.
     *
     * @param args command line arguments (ignored)
     * @throws Exception if an error occurs
     */
    public static void main(String[] args) throws Exception {
        FirstHop firstHop = new FirstHop();
        firstHop.run();
        firstHop.finish();
    }

    private void finish() throws IOException {
        fw.close();
    }

    public void run() throws Exception {
        ArrayList<Runner> myRunners = new ArrayList<Runner>();
        // Obtain the query manager for the session via the workspace ...
        javax.jcr.query.QueryManager queryManager = session.getWorkspace().getQueryManager();

        // Create a query object ...
        String expression = "select * from nt:base " +
                "where jcr:path like '/content/dam/images/hgtv/BUCKET/YEAR/MONTH/%'" +
                "  and crxdao:type = 'FASTFWD'" +
                "  and crxdao:assetType = 'IMAGE'";

        String[] buckets = {"fullset"};
//        String[] buckets = {"unsized"};
//        String[] years = {"2002", "2003", "2004", "2005", "2006", "2007",
//                "2008", "2009", "2010", "2011", "2012", "2013", "2014"};
        String[] years = {"2014", "2013"};
        String[] months = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
        for (String year : years) {
            for (String bucket : buckets) {
                for (String month : months) {
                    String queryString = expression.replaceAll("BUCKET", bucket);
                    queryString = queryString.replaceAll("YEAR", year);
                    queryString = queryString.replaceAll("MONTH", month);
                    Query query = queryManager.createQuery(queryString, Query.SQL);

                    Runner runner = new Runner(query);
                    myRunners.add(runner);
                    runner.start();
                }
            }
        }

        //wait or all runners to complete
        for (Runner r : myRunners) {
            r.join();
        }

        session.logout();
    }

    private class Runner extends Thread {
        private Query query;

        private Runner(Query query) {
            this.query = query;
        }

        public void run() {
            RowIterator rows;
            // Execute the query and get the results ...
            try {
                QueryResult result = this.query.execute();
                rows = result.getRows();
                while (rows.hasNext()) {
                    Row row = rows.nextRow();
                    if (row != null) {
                        Node n = row.getNode();
                        if (n != null) {
                            synchronized (fw) {
                                fw.append(n.getPath())
                                        .append('\t')
                                        .append(n.hasProperty("sni:fastfwdId") ? n.getProperty("sni:fastfwdId").getString() : "WJM")
                                        .append('\n');
                            }
                        }
                    }
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
