package wjm;

import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;
import javax.jcr.query.*;
import java.io.IOException;

/**
 * Created by mckeehan on 10/10/14.
 */
public class UpdateEpisodes {
    private final SimpleCredentials creds = new SimpleCredentials("161151", "Wkq2P#fu".toCharArray());
    private final Repository repository;
    private final String repoUrl = "http://author.hgtv-prod2.sni.hgtv.com/crx/server";
    private final Session session;

    public UpdateEpisodes() throws IOException, RepositoryException {
        this.repository = JcrUtils.getRepository(repoUrl);
        session = repository.login(creds);
    }

    /**
     * The main entry point of the example application.
     *
     * @param args command line arguments (ignored)
     * @throws Exception if an error occurs
     */
    public static void main(String[] args) throws Exception {
        UpdateEpisodes firstHop = new UpdateEpisodes();
        firstHop.run();
    }

    public void run() throws Exception {
        // Obtain the query manager for the session via the workspace ...
        QueryManager queryManager = session.getWorkspace().getQueryManager();

        // Create a query object ...
        String queryString;
        Query query;
        // Execute the query and get the results ...
        RowIterator rows;
        QueryResult brokenPages;
        Row brokenRow;
        Node brokenContentNode;
        Node parentContentNode;
        int count_found = 0;
        int count_fixed = 0;

        queryString = "/jcr:root/content/hgtv-com/en/shows/on-tv//*[jcr:contains(@episodeCode, 'sni:episodeNo')]";
        query = queryManager.createQuery(queryString, Query.XPATH);
        brokenPages = query.execute();
        rows = brokenPages.getRows();

        while (rows.hasNext()) {
            count_found++;
            brokenRow = rows.nextRow();
            if (brokenRow != null) {
                brokenContentNode = brokenRow.getNode();
                if (brokenContentNode != null) {
                    if( brokenContentNode.hasProperty("sni:assetLink")) {
                        String assetPath = brokenContentNode.getProperty("sni:assetLink").getString();
                        Node assetNode = session.getNode(assetPath + "/jcr:content");

                        String episodeCode = assetNode.getProperty("sni:episodeNo").getString() ;

                        String matchingValue = episodeCode.replaceAll("[^\\d.]", "");
                        Integer eNum = Integer.parseInt(matchingValue) % 100;
                        Integer sNum = Integer.parseInt(matchingValue) / 100;
                        String episodeNumber = eNum.toString();
                        String seasonNumber = sNum.toString();

                        brokenContentNode.setProperty("episodeECISRecord", assetPath );
                        brokenContentNode.setProperty("jcr:title", assetNode.getProperty("jcr:title").getString() );
                        brokenContentNode.setProperty("episodeCode", episodeCode );
                        brokenContentNode.setProperty("seasonNumber", seasonNumber );
                        brokenContentNode.setProperty("episodeNumber", episodeNumber );

                        count_fixed++;
                        System.out.println(brokenContentNode.getPath());
                    }
                }
            }
        }
        System.out.printf("Found: %d\nFixed: %d", count_found, count_fixed);
//        session.save();
        session.logout();
    }

}
